import numpy as np
import pandas as pd
import matplotlib; matplotlib.rcParams["savefig.directory"] = "."
from matplotlib import pyplot as plt
import json

feature_names = ["UCS", "density", "friction", "radius", "E", "nu"]
feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]

blast_3 = np.load("blasting_3_unknowns/Yrun_IDEAL_ANFO_800_800_0_1.txt.txt.npy")



def read_JSON_to_XY(filename, feature_names, label_names):
    with open(filename,"r") as f:
        data=json.load(f)
    X=[]
    Y=[]
    for d in data:
        fl, ll = [], []
        for f in feature_names:
            fl.append(d["in"][f])
        for l in label_names:
            ll.append(d["out"][l])
        X.append(fl)
        Y.append(ll)
    return np.array(X), np.array(Y)



X,Y = read_JSON_to_XY("blasting_5_unknowns/five_var_run_IDEAL_ANFO_800_800_0_1.txt.txt", feature_names, label_names)

Y[:,1] /= X[:,3] # normalize final radius
Y[:,2] /= X[:,3] # normalize plastic radius

blast_5 = Y
for i in range(7):
    plt.subplot(211)
    plt.hist(blast_3[:,i], bins=100)
    plt.xlabel(label_names[i])
    plt.subplot(212)
    plt.xlabel(label_names[i])
    plt.hist(blast_5[:,i], bins=100)
    plt.show()
