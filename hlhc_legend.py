import pylab
fig = pylab.figure()
figlegend = pylab.figure(figsize=(3,2))
ax = fig.add_subplot(111)
lines =  ax.plot(range(3),range(3), "o")
lines += ax.plot(range(3),range(3), "*")
lines += ax.plot(range(3),range(3), "v")
lines += ax.plot(range(3),range(3), "+")

figlegend.legend(lines, ('$2^2$', '$2^3$', '$2^4$', '$2^5$'), 'center')
fig.show()
figlegend.show()
figlegend.savefig('legend.png')
