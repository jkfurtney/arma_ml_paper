import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import json
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from scipy.spatial  import cKDTree

def read_JSON_to_XY(filename, feature_names, label_names):
    with open(filename,"r") as f:
        data=json.load(f)
    X=[]
    Y=[]
    for d in data:
        fl, ll = [], []
        for f in feature_names:
            fl.append(d["in"][f])
        for l in label_names:
            ll.append(d["out"][l])
        X.append(fl)
        Y.append(ll)
    return np.array(X), np.array(Y)


feature_names = ["UCS", "density", "friction", "radius", "E", "nu"]
feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]

X,Y = read_JSON_to_XY("five_var_run_IDEAL_ANFO_800_800_0_1.txt.txt", feature_names, label_names)

np.save("X.npy", X)
np.save("Y.npy", Y)
