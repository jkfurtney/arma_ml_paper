import numpy as np
import pylab as pl
from scipy.integrate import odeint
from wmbg import WilliamsburgEoS


class access_control(object):
    """ A simple attribute setting access control mechanism: after
    calling self.lock() new attribtes cannot be set but old
    attributes can still be given a new value. Calling self.unlock()
    allows new attributes to be assigned again

    This class was created to stop client code from adding new
    attributes unless they really mean it. I have had several bugs
    where a typo caused a new instance attribute to be created when
    the intention was to modify an existing attribute.
    """
    def __setattr__(self, key, value):
        if not hasattr(self, "_lock_attributes"):
            self.__dict__[key] = value
        else:
            if hasattr(self, key):
                self.__dict__[key] = value
            else:
                raise Exception("No existing attribute: %s" % key)

    def is_locked(self):
        """
        returns True if setting new attributes in locked
        """
        return hasattr(self, "_lock_attributes")

    def lock(self):
        """
        after calling this function new class attributes can not be
        assigned. Call unlock() to reverse
        """
        if not self.is_locked():
            self._lock_attributes = True

    def unlock(self):
        if self.is_locked():
            del(self._lock_attributes)


class simple_model(access_control):
    def __init__(self):
        self.explosive_file = ""
        self.p_eq = 0.0
        self.breakout_angle = 90.0
        self.bench_height   = 10.0
        self.burden         = 3.0
        self.rock_density   = 2600.0
        self.hole_length    = 7.5
        self.hole_diameter  = 0.1  # initial diameter
        self.explosive_diameter = self.hole_diameter
        self.lock()

    def setup(self):
        assert not self.explosive_file == "", "specify an explosive file"
        assert not self.p_eq == 0.0, "specify an eq. pressure"

        self.unlock()
        self.EoS = WilliamsburgEoS(self.explosive_file)
        self.rv_exp = 1  # self.EoS.reducedVolumeAtExplosionPressure()
        self.rv_eq = self.EoS.reducedVolumeAtPressure(self.p_eq)

        # self.p_cj = self.EoS(1.0)
        self.p_exp = self.EoS(self.rv_exp)

        self.rad_0 = self.explosive_diameter/2.0
        self.rad_eq = np.sqrt(self.rv_eq * self.rad_0 * self.rad_0)

        self.volume_eq = self.hole_length * np.pi * self.rad_eq**2
        self.volume_exp = self.hole_length * np.pi * self.rad_0**2

        w = 2 * self.burden * np.tan(np.deg2rad(self.breakout_angle / 2))
        self.burden_volume = 0.5 * self.bench_height * self.burden * w
        self.burden_mass = self.rock_density * self.burden_volume

        self.lock()

    def solve(self, end_time=1.0):
        if not hasattr(self, "EoS"):
            self.setup()
        # params describing gas velocity as a function of burden displacement.
        vf_c  = 342.0
        vf_Q  = 0.1244
        vf_B  = 3.681
        vf_M  = -0.850
        vf_nu = 2.91
        sina = np.sin(np.deg2rad(self.breakout_angle / 2.0))
        phi0 = 0.15
        d0   = 2 * self.rad_eq

        def area(x):
            """ area of pressure application """
            return (2 * self.rad_eq + 0*x) * self.hole_length

        def phi(x):
            return 1 - (1 - phi0) * (d0 / (x + d0))

        def pressure(vol):
            return max(1e-6, self.EoS(vol / self.volume_eq * self.rv_eq) - 1e5)

        def acceleration(vol, x):
            return pressure(vol) * area(x) / self.burden_mass

        def vf(x):
            """ gas velocity in fracture """
            if x == 0:
                return 0
            return vf_c/(1+vf_Q*np.exp(-vf_B*(np.log10(sina*x)-vf_M))) \
                ** (1/vf_nu)

        def dvfdt(x):
            """ gas volume lost to fracture flow. """
            return self.hole_length * x * sina * vf(x)

        def dvdt(u):
            """ gas cavity volume """
            return 2 * self.rad_eq * u * self.hole_length

        def dvsdt(x):
            """ stemming volume venting """
            return vf_c * (np.pi * self.rad_eq ** 2 + 2 * self.rad_eq * x) * \
                phi(x)

        x0 = [0.0, 0.0, self.volume_eq, 0, 0]  # initial conditions

        def deriv(x, t):
            # x is position
            # x[0] is x position
            # x[1] is x' vel
            # x[2] is gas cavity, fracture and stemming volume
            position = x[0]
            velocity = x[1]
            v_c = x[2]
            v_f = x[3]
            v_s = x[4]
            volume = v_c + v_f + v_s
            return [velocity,
                    acceleration(volume, position),
                    dvdt(velocity),
                    dvfdt(position),
                    dvsdt(position)]

        time = [0.0] + np.logspace(-4, np.log10(end_time), 1000).tolist()

        res = odeint(deriv, x0, time)
        self.unlock()
        self.disp, self.vel = res[:, 0], res[:, 1]
        self.v_c, self.v_f, self.v_s = res[:, 2], res[:, 3], res[:, 4]
        self.vol_total = res[:, 2] + res[:, 3] + res[:, 4]
        self.time = time
        self.lock()

    def energy(self):
        """
        calculate energy balance terms
        """
        assert hasattr(self, "vel"), " solve() model first"
        rv_atmo = self.EoS.reducedVolumeAtPressure(1e5)

        # calculate total expansion energy of explosive gas
        totalExpansionEnergy = self.volume_exp * \
            self.EoS.expansionEnergyPerUnitVolume(self.rv_exp, rv_atmo)

        plasticFlowEnergy = self.volume_exp * \
            self.EoS.expansionEnergyPerUnitVolume(self.rv_exp, self.rv_eq)

        expansionWork0 = self.volume_eq * \
            self.EoS.expansionEnergyPerUnitVolume(self.rv_eq, rv_atmo)

        expansionWork = totalExpansionEnergy - plasticFlowEnergy
        np.testing.assert_allclose(expansionWork, expansionWork0)

        ke = self.burden_mass * 0.5 * self.vel ** 2

        v_total = self.v_c + self.v_f + self.v_s
        e_cavity = []
        for vt, vc in zip(v_total, self.v_c):
            pressure = self.EoS(vt / self.volume_eq * self.rv_eq)-1e5
            rv_now = self.EoS.reducedVolumeAtPressure(pressure)
            if rv_now > rv_atmo:
                energy_now = 0
            else:
                energy_now = vc*self.EoS.expansionEnergyPerUnitVolume(rv_now,
                                                                      rv_atmo)
            e_cavity.append(energy_now)
        e_cavity = np.array(e_cavity)

        dvs = np.roll(self.v_s, -1) - self.v_s
        dvs[-1] = dvs[-2]
        dvf = np.roll(self.v_f, -1) - self.v_f
        dvf[-1] = dvf[-2]

        press = []
        e_stemming, e_frac = [], []
        frac_energy_now, stem_energy_now = 0, 0
        for vt, vs, vf in zip(v_total, dvs, dvf):
            pressure = self.EoS(vt / self.volume_eq * self.rv_eq)
            press.append(pressure)
            rv_now = self.EoS.reducedVolumeAtPressure(pressure)
            if rv_now > rv_atmo:
                pass
            else:
                stem_energy_now += vs * \
                    self.EoS.expansionEnergyPerUnitVolume(rv_now, rv_atmo)
                frac_energy_now += vf * \
                    self.EoS.expansionEnergyPerUnitVolume(rv_now, rv_atmo)
            e_stemming.append(stem_energy_now)
            e_frac.append(frac_energy_now)

        # why is there a factor of 1/2 here?
        e_stemming = np.array(e_stemming) / 2.
        e_frac = np.array(e_frac) / 2.

        self.unlock()
        self.totalExpansionEnergy = totalExpansionEnergy
        self.plasticFlowEnergy = plasticFlowEnergy
        self.ke_time = ke
        self.frac_time = e_frac  # expansionWork0 - e_cavity - ke
        self.stemming_time = e_stemming
        self.cavity_pe_time = e_cavity
        self.pressure = np.array(press)
        self.lock()

        ePlastic  = plasticFlowEnergy / totalExpansionEnergy * 100.0
        eFracture = e_frac[-1] / totalExpansionEnergy * 100.0
        eStemming = e_stemming[-1] / totalExpansionEnergy * 100
        eKE       = ke[-1] / totalExpansionEnergy * 100.0

        print("Energy Partition:-------------------------------------")
        print("------------------------------------------------------")
        print("Plastic flow loss               %", ePlastic)
        print("Energy lost to fracture venting %", eFracture)
        print("Energy lost to stemming venting %", eStemming)
        print("Burden kinetic energy:          %", eKE)
        print("------------------------------------------------------")
