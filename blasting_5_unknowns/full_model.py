from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})
import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import json
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from scipy.spatial  import cKDTree
import joblib

X = np.load("X.npy")
Y = np.load("Y.npy")
burden_min, burden_max = 1.0, 15.0
charge_fraction_min, charge_fraction_max = 0.4, 1.0
min_vol = burden_min*burden_min*1/charge_fraction_max
max_vol = burden_max*burden_max*1/charge_fraction_min
bvol = np.linspace(min_vol, max_vol, X.shape[0])

vel = np.load("vel.npy")
assert vel.shape == bvol.shape

feature_names = ["UCS", "density", "friction", "radius", "E", "nu", "bvol"]
feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu", "bvol"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt", "vel"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt", "bvel"]

X = np.append(X, bvol[...,None], 1)
Y = np.append(Y, vel[...,None], 1)

Y[:,1] /= X[:,3] # normalize final radius
Y[:,2] /= X[:,3] # normalize plastic radius

for i, fn in enumerate(feature_names):
    print(fn, X[:,i].min(), X[:,i].max())

sizes = [8, 16, 48, 112, 240, 496, 1008, 2032, 4080, 8176] # 8176, 16368, 32752, 65520

def train_and_test(X_train, y_train,
                 X_test, y_test):
    scaler = skl.preprocessing.StandardScaler()
    yscaler = skl.preprocessing.StandardScaler()
    scaler.fit(X_train)
    yscaler.fit(y_train)

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_train_scaled = yscaler.transform(y_train)
    y_test_scaled = yscaler.transform(y_test)
    hl_size = 15
    mlpr = MLPRegressor(hidden_layer_sizes=(hl_size,hl_size,hl_size),
                        activation='tanh',
                        solver='lbfgs',
                        alpha=1e-4,
                        max_iter=2*1600,
                        random_state=1)
    target = -1
    mlpr.fit(X_train_scaled, y_train_scaled[:,target])
    ts = mlpr.score(X_train_scaled, y_train_scaled[:,target])
    vs = mlpr.score(X_test_scaled, y_test_scaled[:,target])
    return mlpr, ts, vs, scaler, yscaler

# first fit all the data
x_train, x_test, y_train, y_test = train_test_split(X, Y,
                                                    test_size=0.25, random_state=2)
print(X.shape, Y.shape)
mlpr, ts, vs, scaler, yscaler = train_and_test(x_train, y_train, x_test, y_test)

joblib.dump((scaler, yscaler, mlpr), "burden_vel_model.pkl")

res = mlpr.predict(scaler.transform(x_test))
res = np.array((res,res,res,res,res,res,res,res)).T
pred = yscaler.inverse_transform(res)[:,-1]
act = np.array(y_test)[:,-1]

plt.hist(100*(pred-act)/act, bins=100)
plt.ylabel("Bin count")
plt.xlabel("Error [%]")
plt.title("Prediction error")
plt.show()


def show_actual_predicted(actual, predicted):
    assert len(actual.shape)==1
    assert len(predicted.shape)==1

    fig = plt.figure(constrained_layout=True, figsize=(15, 10))
    gs = fig.add_gridspec(3, 5) # vert the horiz
    ax1 = fig.add_subplot(gs[0:2, 0:2]) # scatter dim
    ax2 = fig.add_subplot(gs[0:2, 2:4]) # scatter percent
    ax3 = fig.add_subplot(gs[0:2, 4]) # target hist
    ax4 = fig.add_subplot(gs[2, :2]) # dimensional error hist
    ax5 = fig.add_subplot(gs[2, 2:4]) # percent error hist
    #ax6 = fig.add_subplot(gs[2, 4]) # text input


    error = actual-predicted
    data_bc_error = (error) / actual
    data_bc_error = data_bc_error * 100

    points = np.array([error, actual]).T
    dist = np.log10(np.mean(cKDTree(points).query(points, k=100)[0], axis=1))
    order = np.argsort(dist)[::-1]
    dist = dist[order]
    p_error = np.array(error)[order]
    p_actual = np.array(actual)[order]
    ax1.scatter(p_error, p_actual, c=dist, cmap=plt.cm.get_cmap('jet').reversed(), s=2)
    ax1.set_ylabel("Actual []")
    ax1.set_xlabel("Error []")
    ax1.axvline(0, linestyle='--', color='k', lw=1.5)

    points = np.array([data_bc_error, actual]).T
    dist = np.log10(np.mean(cKDTree(points).query(points, k=100)[0], axis=1))
    order = np.argsort(dist)[::-1]
    dist = dist[order]
    p_error = np.array(data_bc_error)[order]
    p_actual = np.array(actual)[order]
    ax2.scatter(p_error, p_actual, c=dist, cmap=plt.cm.get_cmap('jet').reversed(), s=2)
    ax2.set_ylabel("Actual []")
    ax2.set_xlabel("Error %")
    ax2.axvline(0, linestyle='--', color='k', lw=1.5)


    ax3.axvline(0, linestyle='--', color='k', lw=1.5)
    ax3.grid(axis='y')
    ax3.set_xlabel('Count ')
    ax3.set_ylabel('Actual []')



    ax3.hist(actual, bins=200, orientation="horizontal");

    ax4.hist(actual-predicted, bins=200)
    ax4.axvline(0, linestyle='--', color='k', lw=1.5)
    ax4.set_xlabel('Error []')
    ax4.set_ylabel('Count')

    ax5.hist(data_bc_error, bins=200)
    ax5.axvline(0, linestyle='--', color='k', lw=1.5)
    ax5.set_xlabel('Error %')
    ax5.set_ylabel('Count')
    plt.show()


show_actual_predicted(act[:,-1],pred[:,-1])


points = np.array((pred,act)).T
dist = np.log10(np.mean(cKDTree(points).query(points,k=100)[0],axis=1))
order = np.argsort(dist)[::-1]
dist=dist[order]
pred=np.array(pred)[order]
act=np.array(act)[order]
plt.xlabel("Actual target [m/s]")
plt.ylabel("Predicted target [m/s]")
ticks = [0,10,30,50]
plt.xticks(ticks)
plt.title("Predicted vs Actual\nBurden Velocity")
plt.yticks(ticks)
plt.plot([0, max(ticks)], [0, max(ticks)], "--", color="black")
plt.xlim(5,50)
plt.ylim(5,50)
plt.scatter(pred, act, c=dist, cmap=plt.cm.get_cmap('jet').reversed())
plt.gca().set_aspect(1)
plt.show()
