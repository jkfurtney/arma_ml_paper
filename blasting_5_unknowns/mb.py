import numpy as np
import pandas as pd
import matplotlib; matplotlib.rcParams["savefig.directory"] = "."
from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})
import joblib

model,s,ys = joblib.load("m.pkl")


# UCS 10009281.803223256 199986636.7794857
# density 1800.0173682265913 2999.920088017391
# friction 25.001210393054873 54.99915913904264
# radius 0.04000087727240182 0.14999948844066402
# E 5001300420.394139 49999854936.287636
# nu 0.25 0.25

UCS = 1e8
rho = 2_600.0
friction = 45
radius = 0.1
E = 2e10
nu = 0.25
values = [UCS, rho, friction, radius, E, nu]

N = 400
X = np.zeros((N,6))
for i, v in enumerate(values):
    X[:,i]=v

X[:,3] = np.linspace(0.05,0.149,N)
target = 5 # vibrations
a = model.predict(s.transform(X))
Yprime = np.zeros((N,ys.scale_.shape[0]))
Yprime[:,target] = a
Y = ys.inverse_transform(Yprime)[:,target]

model,s,ys = joblib.load("mt3.pkl")

target = 3 # vibrations
a = model.predict(s.transform(X))
Yprime = np.zeros((N,ys.scale_.shape[0]))
Yprime[:,target] = a
Y3 = ys.inverse_transform(Yprime)[:,target]

plt.plot(np.linspace(0.05,0.149,N), Y)
plt.plot(np.linspace(0.05,0.149,N), Y3)
plt.show()
