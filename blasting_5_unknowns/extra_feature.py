from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})

import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import json
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from scipy.spatial  import cKDTree

X = np.load("X.npy")
Y = np.load("Y.npy")
burden_min, burden_max = 1.0, 15.0
charge_fraction_min, charge_fraction_max = 0.4, 1.0
min_vol = burden_min*burden_min*1/charge_fraction_max
max_vol = burden_max*burden_max*1/charge_fraction_min
bvol = np.linspace(min_vol, max_vol, X.shape[0])

vel = np.load("vel.npy")
assert vel.shape == bvol.shape

feature_names = ["UCS", "density", "friction", "radius", "E", "nu", "bvol"]
feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu", "bvol"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt", "vel"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt", "bvel"]

X = np.append(X, bvol[...,None], 1)
Y = np.append(Y, vel[...,None], 1)

Y[:,1] /= X[:,3] # normalize final radius
Y[:,2] /= X[:,3] # normalize plastic radius

for i, fn in enumerate(feature_names):
    print(fn, X[:,i].min(), X[:,i].max())

sizes = [8, 16, 48, 112, 240, 496, 1008, 2032, 4080, 8176] # 8176, 16368, 32752, 65520

def train_and_test(X_train, y_train,
                 X_test, y_test):
    scaler = skl.preprocessing.StandardScaler()
    yscaler = skl.preprocessing.StandardScaler()
    scaler.fit(X_train)
    yscaler.fit(y_train)

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_train_scaled = yscaler.transform(y_train)
    y_test_scaled = yscaler.transform(y_test)
    hl_size = 15
    mlpr = MLPRegressor(hidden_layer_sizes=(hl_size,hl_size,hl_size),
                        activation='tanh',
                        solver='lbfgs',
                        alpha=1e-4,
                        max_iter=2*1600,
                        random_state=1)
    target = 0
    mlpr.fit(X_train_scaled, y_train_scaled[:,target])
    ts = mlpr.score(X_train_scaled, y_train_scaled[:,target])
    vs = mlpr.score(X_test_scaled, y_test_scaled[:,target])
    return mlpr, ts, vs, scaler, yscaler

# first fit all the data
x_train, x_test, y_train, y_test = train_test_split(X, Y,
                                                    test_size=0.25, random_state=2)
print(X.shape, Y.shape)
print(train_and_test(x_train, y_train, x_test, y_test))


lhc_sizes = range(3, 13)
X_train, X_test = [], []
Y_train, Y_test = [], []
test_score, validation_score = [], []

indices = np.array(range(len(X)))
np.random.shuffle(indices)

for lhc_size in sizes:
    print(f"training {lhc_size}")
    size = lhc_size
    current = indices[:size]
    indices = indices[size:]
    new_X, new_y = X[current], Y[current]
    x_train, x_test, y_train, y_test = train_test_split(new_X, new_y,
                                                        test_size=0.25, random_state=2)
    X_train += x_train.tolist()
    X_test += x_test.tolist()
    Y_train += y_train.tolist()
    Y_test += y_test.tolist()

    model, ts, vs, s, ys = train_and_test(np.array(X_train), np.array(Y_train),
                                   np.array(X_test), np.array(Y_test))

    test_score.append(ts)
    validation_score.append(vs)

validation_score = np.array(validation_score)
test_score = np.array(test_score)
plt.semilogx(sizes, np.log10(1-test_score), "o-")
plt.semilogx(sizes, np.log10(1-validation_score), "o-")
plt.ylabel("Model score (log10(1-$r^2$))")
plt.xlabel("Number of samples []")
plt.legend(["Test score", "Training score"])
print((sizes, test_score, validation_score))



res = model.predict(s.transform(X_test))
#res = np.array((res,res,res,res,res,res,res)).T
pred = ys.inverse_transform(res)[:,-1]
act = np.array(Y_test)[:,0]

plt.rcParams.update({'font.size': 18})

plt.hist((pred-act)/act*100, bins=100)
plt.xlabel("Error [%]")
plt.title("Blasting 5")
plt.ylabel("Bin count []")
plt.show()
w1p = (abs((pred-act)/act)<0.01).sum()/len(pred)
# 92%
w2p  = (abs((pred-act)/act)<0.02).sum()/len(pred)
# 97%
max_p_err = ((pred-act)/act)[np.argmax((abs((pred-act)/act)))]*100
# -7%

points = np.array((pred,act)).T
dist = np.log10(np.mean(cKDTree(points).query(points,k=100)[0],axis=1))
order = np.argsort(dist)[::-1]
dist=dist[order]
pred=np.array(pred)[order]
act=np.array(act)[order]
plt.xlabel("Actual target [MPa]")
plt.ylabel("Predicted target [MPa]")
ticks = [0,200,400,600]
plt.xticks(ticks)
plt.title("Blasting 5")
plt.yticks(ticks)
plt.plot([0, max(ticks)], [0, max(ticks)], "--", color="black")
plt.scatter(pred/1e6, act/1e6, c=dist, cmap=plt.cm.get_cmap('jet').reversed())
plt.gca().set_aspect(1)
plt.show()


plt.plot(pred/1e6, act/1e6, "o")
plt.title("Blasting 5")
plt.xlabel("Actual target [MPa]")
plt.ylabel("Predicted target [MPa]")
ticks = [0,200,400,600]
plt.plot([0, max(ticks)], [0, max(ticks)], color="black")
plt.xticks(ticks)
plt.yticks(ticks)
plt.gca().set_aspect(1)
plt.show()
