import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import json
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from scipy.spatial import cKDTree
import joblib

(scaler, yscaler, mlpr) = joblib.load('../2023_blasting_model/IDEAL_HeavyANFO80%prill_1000_0_100.txt_scalers_mlpr.pkl')

print(' ')
print(mlpr.coefs_)
print(' ')
print(mlpr.intercepts_)
print(' ')
print(scaler.scale_)
print(' ')
print(scaler.mean_)
print(' ')
print(yscaler.scale_)
print(' ')
print(yscaler.mean_)

# feature_names = ["UCS", "density", "friction", "radius", "E", "nu"]
# feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu"]
#
# label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
# label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]
#
#
# X = np.load("X.npy")
# Y = np.load("Y.npy")
#
# # bench height 3 to 30 m
# # burden 1 to 15 m
# # max burden area 15*15 225 m2 lowest is 1 m2
# # since this is one dimensional we can just use a single linear space, no lhc needed.
# burden_min, burden_max = 1.0, 15.0
# charge_fraction_min, charge_fraction_max = 0.4, 1.0
# min_vol = burden_min*burden_min*1/charge_fraction_max
# max_vol = burden_max*burden_max*1/charge_fraction_min
# bvol = np.linspace(min_vol, max_vol, X.shape[0])
#
# # now calculate a new Y which is the burden velocity
# bvol_test = bvol.mean()
# from isee_burden_model import simple_model
#
# explosiveFN   = 'IDEAL_ANFO_800_800_0_1.txt'
# holeP0        = 323.2e6            # Pa  "equilibrium" pressure
#
# breakoutAngle = 90.0             # deg
# benchHeight   = 10.0             # m
# burden        = 3.0              # m
# rockDensity   = 2600.0           # kg/m^3
# holeHeight    = 7.5              # m
# holeR00       = 0.05             # initial radius of hole.
#
# model = simple_model()
# model.explosive_file = "exp.txt"
# model.p_eq = holeP0
# model.rock_density = rockDensity
# model.explosive_diameter = model.hole_diameter = holeR00*2
# model.burden = 3
# model.bench_height = 10
#
# model.setup()
# model.solve()
# vel.append(model.vel[-1])
