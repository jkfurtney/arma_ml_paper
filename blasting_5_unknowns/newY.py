import numpy as np
from isee_burden_model import simple_model

feature_names = ["UCS", "density", "friction", "radius", "E", "nu"]
feature_names2 = ["UCS", "rho", "phi", "r", "E", "nu"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]


N = 10_000

# bench height 3 to 30 m
# burden 1 to 15 m
# max burden area 15*15 225 m2 lowest is 1 m2
# since this is one dimensional we can just use a single linear space, no lhc needed.
burden_min, burden_max = 1.0, 15.0
charge_fraction_min, charge_fraction_max = 0.4, 1.0
min_vol = burden_min*burden_min*1/charge_fraction_max
max_vol = burden_max*burden_max*1/charge_fraction_min
bvol = np.linspace(min_vol, max_vol, X.shape[0])

# now calculate a new Y which is the burden velocity

vel = []
for i in range(N):
    if i%100==0 and i: print(i, max(vel), min(vel))
    model = simple_model()
    model.explosive_file = "exp.txt"
    model.p_eq = Y[i][0]
    model.rock_density = X[i][1]
    model.explosive_diameter = model.hole_diameter = X[i][3]*2
    model.hole_length = 1
    model.bench_height = 1
    model.setup()
    model.burden_mass = X[i][2]*bvol[i]
    model.solve()
    vel.append(model.vel[-1])
# np.save()...
