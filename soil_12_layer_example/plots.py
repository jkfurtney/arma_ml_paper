import itasca as it
it.command("python-reset-state false")
from itasca import zonearray as za
from itasca import gridpointarray as gpa
import numpy as np

save_name = "21-62-22127_CPT17-BSC.XLS_60.0.f3sav"
it.command(f"model restore '{save_name}'")


# normalize displacement
gpa.set_disp(gpa.disp()/np.linalg.norm(gpa.disp(), axis=1).max())

# convert cohesion to SI
psf = 47.88   # psf to Pa
za.set_prop_scalar("cohesion", za.prop("cohesion")*psf)