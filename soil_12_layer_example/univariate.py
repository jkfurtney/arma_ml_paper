import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
np.random.seed(12344)
import pylab as plt

import pickle
with open("raw_dict.pkl", "rb") as f:
    mdata = pickle.load(f)


assert False, "I never finished this, there is a mistake somewhere in the univariate analysis."

final_load = []
raw_parameters = []
seconds = 0
for value in mdata.values():
    final_load.append(value["result"]["final_load"])
    raw_parameters.append(value["parameters"]["raw_parameters"] +
                          value["parameters"]["friction_angle_array"] +
                          value["parameters"]["cohesion_array"])
Y = np.array(final_load)
X = np.array(raw_parameters)
X = X[:, 12:] # skip the raw parameters

from minepy import MINE

data=[]
for i in range(X.shape[1]):
    local_data=[]
    for j in range(Y.shape[0]):
        m = MINE()
        m.compute_score(X[:,i], Y)
        score = m.mic()
        print(f"feature {i}, target {j}: {score}")
        local_data.append(score)
    data.append(local_data)

from scipy.stats import pearsonr
pdata=[]
for i in range(X.shape[1]):
    local_data=[]
    for j in range(Y.shape):
        m.compute_score(X[:,i], Y)
        score = pearsonr(X[:,i], Y)
        print(f"feature {i}, target {j}: {score}")
        local_data.append(score)
    pdata.append(local_data)
# note that this one is linear only.

# see https://gist.github.com/josef-pkt/2938402
import numpy as np

def dcov_all(x, y):
    def dist(x, y): return np.abs(x[:, None] - y)

    def d_n(x):
        d = dist(x, x)
        dn = d - d.mean(0) - d.mean(1)[:,None] + d.mean()
        return dn

    dnx = d_n(x)
    dny = d_n(y)

    denom = np.product(dnx.shape)
    dc = (dnx * dny).sum() / denom
    dvx = (dnx**2).sum() / denom
    dvy = (dny**2).sum() / denom
    dr = dc / (np.sqrt(dvx) * np.sqrt(dvy))
    return dc, dr, dvx, dvy

from scipy.stats import pearsonr
ddata=[]
for i in range(X.shape[1]):
    local_data=[]
    for j in range(Y.shape[0]):
        m.compute_score(X[:,i], Y)
        score = dcov_all(X[:,i], Y)
        print(f"feature {i}, target {j}: {score}")
        local_data.append(score)
    ddata.append(local_data)
