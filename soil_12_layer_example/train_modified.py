import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

fc_map = np.poly1d([-1.49603151e+00,  1.83220575e+02, -7.72644152e+03,  1.38367935e+05, -8.78011290e+05])
def map_fric_to_coh(fric):
    if fric==0: return 0
    return fc_map(fric)

Xori, Yori = np.load("X2.npy"), np.load("Y.npy")
new_feature = np.vectorize(map_fric_to_coh)(Xori[:,:12]) + Xori[:,12:]
Xori = np.hstack((Xori, new_feature))
valid_indexes = np.random.choice(Xori.shape[0], int(Xori.shape[0]/10), replace=False) # we remove 10% as validation
Xini, X_valid, Yini, Y_valid = train_test_split(Xori, Yori , test_size=0.25, random_state=2) # here valid set are used for accuracy independently of training set size
scaler = skl.preprocessing.StandardScaler()
yscaler = skl.preprocessing.PowerTransformer() #StandardScaler()
scaler.fit(Xini)
yscaler.fit(Yini.reshape(-1,1))
X_valid_scaled = scaler.transform(X_valid)
y_valid_scaled = yscaler.transform(Y_valid.reshape(-1,1)).ravel()
#X[:12] = np.tan(np.deg2rad(X[:12]))

samples = [] # number of samples tested
accuracy = [] # accuracy achieved
top2p = [] # % of top 2 %results

for nb_samples in range(10000,Xini.shape[0]+1,10000):
    samples.append(nb_samples)
    indexes = np.random.choice(Xini.shape[0], nb_samples, replace=False)   # training on a subset
    X_train = Xini[indexes]  # data used for training
    Y_train = Yini[indexes] # data used for training


    #scaler.fit(X_train)
    #yscaler.fit(y_train.reshape(-1,1))

    X_train_scaled = scaler.transform(X_train)
    y_train_scaled = yscaler.transform(Y_train.reshape(-1,1)).ravel()

    def score(model):
        ts = model.score(X_train_scaled, y_train_scaled)
        vs = model.score(X_valid_scaled, y_valid_scaled)
        print("score of training data", ts)
        print("score of test data", vs)
        delta0 = ((y_train_scaled-model.predict(X_train_scaled)))
        print(abs(delta0).max(), abs(delta0).mean(), abs(delta0).min())
        delta1 = ((y_valid_scaled-model.predict(X_valid_scaled)))
        print(abs(delta1).max(), abs(delta1).mean(), abs(delta1).min())

        return delta0, delta1

    mlpr = MLPRegressor(hidden_layer_sizes=(36, 36, 36, 36, 36),
                        activation='tanh',
                        solver='lbfgs',
                        max_iter=2*1600,
                        random_state=2,
                        learning_rate='constant',
                        learning_rate_init=1e-3)

    mlpr.fit(X_train_scaled, y_train_scaled)
    accuracy.append(mlpr.score(X_valid_scaled, y_valid_scaled))
    delta0, delta1 = score(mlpr)
    # plt.subplot(211)
    # plt.hist(100*delta0/(y_train_scaled.max()-y_train_scaled.min()), bins=100)

    #delta1 = (y_test - yscaler.inverse_transform(mlpr.predict(X_test_scaled).reshape((-1,1))).T)/(y_train.max()-y_train.min())
    ep = 100*delta1/(y_train_scaled.max()-y_train_scaled.min())
    # plt.hist(ep, bins=100)

    # mask=abs(ep<10)
    # print (sum(mask)/ep.shape[0]*100, "% of the data fits within 10%")
    # mask=abs(ep<5)
    # print (sum(mask)/ep.shape[0]*100, "% of the data fits within 5%")
    # mask=abs(ep<2)
    # print (sum(mask)/ep.shape[0]*100, "% of the data fits within 2%")
    top2p.append(sum(abs(ep<2))/ep.shape[0]*100)
    #plt.hist(delta0, bins=50)
    #plt.hist(delta1, bins=50)
    # plt.subplot(212)
    # plt.plot(y_train, yscaler.inverse_transform(mlpr.predict(X_train_scaled).reshape((-1,1))), "o")
    # plt.plot(y_test, yscaler.inverse_transform(mlpr.predict(X_test_scaled).reshape((-1,1))), "o")
    # plt.plot([y_train.min(), y_train.max()], [y_train.min(), y_train.max()], color="black")
    # plt.gca().set_aspect(1)
    # plt.show()

plt.subplot(211)
plt.plot(samples,accuracy,'o-')
plt.subplot(212)
plt.plot(samples,top2p,'g-')
plt.savefig('results.svg')
plt.show()