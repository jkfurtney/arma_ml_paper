import numpy as np
import pandas as pd
import matplotlib; matplotlib.rcParams["savefig.directory"] = "."
from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})
from scipy.spatial import cKDTree

def show_actual_predicted(file, name, actual, predicted):
    assert len(actual.shape)==1
    assert len(predicted.shape)==1

    fig = plt.figure(constrained_layout=True, figsize=(15, 10))
    gs = fig.add_gridspec(3, 5) # vert the horiz
    ax1 = fig.add_subplot(gs[0:2, 0:2]) # scatter dim
    ax2 = fig.add_subplot(gs[0:2, 2:4]) # scatter percent
    ax3 = fig.add_subplot(gs[0:2, 4]) # target hist
    ax4 = fig.add_subplot(gs[2, :2]) # dimensional error hist
    ax5 = fig.add_subplot(gs[2, 2:4]) # percent error hist
    ax6 = fig.add_subplot(gs[2, 4]) # text input


    error = actual-predicted
    data_bc_error = (error) / actual
    data_bc_error = data_bc_error * 100

    points = np.array([error, actual]).T
    dist = np.log10(np.mean(cKDTree(points).query(points, k=100)[0], axis=1))
    order = np.argsort(dist)[::-1]
    dist = dist[order]
    p_error = np.array(error)[order]
    p_actual = np.array(actual)[order]
    ax1.scatter(p_error, p_actual, c=dist, cmap=plt.cm.get_cmap('jet').reversed(), s=2)
    ax1.set_ylabel("Actual []")
    ax1.set_xlabel("Error []")
    ax1.axvline(0, linestyle='--', color='k', lw=1.5)

    points = np.array([data_bc_error, actual]).T
    dist = np.log10(np.mean(cKDTree(points).query(points, k=100)[0], axis=1))
    order = np.argsort(dist)[::-1]
    dist = dist[order]
    p_error = np.array(data_bc_error)[order]
    p_actual = np.array(actual)[order]
    ax2.scatter(p_error, p_actual, c=dist, cmap=plt.cm.get_cmap('jet').reversed(), s=2)
    ax2.set_ylabel("Actual []")
    ax2.set_xlabel("Error %")
    ax2.axvline(0, linestyle='--', color='k', lw=1.5)


    ax3.axvline(0, linestyle='--', color='k', lw=1.5)
    ax3.grid(axis='y')
    ax3.set_xlabel('Count ')
    ax3.set_ylabel('Actual []')



    ax3.hist(actual, bins=200, orientation="horizontal");

    ax4.hist(actual-predicted, bins=200)
    ax4.axvline(0, linestyle='--', color='k', lw=1.5)
    ax4.set_xlabel('Error []')
    ax4.set_ylabel('Count')

    ax5.hist(data_bc_error, bins=200)
    ax5.axvline(0, linestyle='--', color='k', lw=1.5)
    ax5.set_xlabel('Error %')
    ax5.set_ylabel('Count')

    ax6.text(0, 0.5, name, fontweight='bold')
    ax6.axis('off')

    plt.suptitle(file)
    #plt.tight_layout()
    plt.savefig(f"{file}_{name}.png")
    plt.clf()
    plt.close(fig)
    #plt.show()


