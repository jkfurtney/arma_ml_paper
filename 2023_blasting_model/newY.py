import numpy as np
import math
from isee_burden_model import simple_model
from load_syntheric_data import read_JSON_to_XY

feature_names = ["UCS", "density", "friction", "radius", "E"]
feature_names2 = ["UCS", "rho", "phi", "r", "E"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]


N = 10_000

# bench height 3 to 30 m
# burden 1 to 15 m
# max burden area 15*15 225 m2 lowest is 1 m2
# since this is one dimensional we can just use a single linear space, no lhc needed.
min_face_angle = np.deg2rad(60)  # [deg]
burden_min, burden_max = 1.0, 15.0
charge_fraction_min, charge_fraction_max = 0.4, 1.0
height_max = 30 # [m] max bench height

# these are areas
min_vol = burden_min*burden_min/charge_fraction_max
max_vol = (burden_max*burden_max + burden_max*height_max/2/math.tan(min_face_angle))/charge_fraction_max
bvol = np.linspace(min_vol, max_vol, 10000)
# loop over each explosive type
# load the corresponding JSON file to get X and Y
# calculate the throw velocity for each case (10000 cases)
# add the throw velocity to the Y vector and save it as a .npy file.
# each explosive file will have an X and Y npy file.

explosive_files = ["IDEAL_ANFO_800_800_0_1.txt", "IDEAL_DE35AT1200_1200_0_1.txt",
                   "IDEAL_HeavyANFO80%prill_1000_0_100.txt", "IDEAL_Surface_Emulsion_1150_1150_0_1.txt"]

#explosive_files = ["IDEAL_HeavyANFO80%prill_1000_0_100.txt"]
for explosive_file in explosive_files:
    datafile_name = "../blasting_5_unknowns/five_var_run_" + explosive_file + ".txt"
    X, Y = read_JSON_to_XY(datafile_name, feature_names, label_names)

    # now calculate a new Y which is the burden velocity

    assert X.shape[0] == 10_000
    vel = []
    for i in range(N):
        if i % 100 == 0 and i:
            print(i, max(vel), min(vel))
        model = simple_model()
        model.explosive_file = explosive_file
        model.p_eq = Y[i][0]
        model.rock_density = X[i][1]
        model.explosive_diameter = model.hole_diameter = X[i][3]*2
        model.hole_length = 1
        model.bench_height = 1
        model.setup()
        model.burden_mass = model.rock_density*bvol[i]
        model.solve()
        vel.append(model.vel[-1])
    # save X and Y with the explosive name added and add vel to Y in each case
    np.save(f"X_{explosive_file}.npy", np.c_[X, bvol])
    np.save(f"Y_{explosive_file}.npy", np.c_[Y, vel])
