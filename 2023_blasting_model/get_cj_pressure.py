from wmbg import WilliamsburgEoS


explosive_files = ["IDEAL_ANFO_800_800_0_1.txt",
                   "IDEAL_DE35AT1200_1200_0_1.txt",
                   "IDEAL_HeavyANFO80%prill_1000_0_100.txt",
                   "IDEAL_Surface_Emulsion_1150_1150_0_1.txt"]

for ef in explosive_files:
    m = WilliamsburgEoS(ef)
    print(ef, m(1))
