import json
import numpy as np
def read_JSON_to_XY(filename, feature_names, label_names):
    with open(filename,"r") as f:
        data=json.load(f)
    X=[]
    Y=[]
    for d in data:
        fl, ll = [], []
        for f in feature_names:
            fl.append(d["in"][f])
        for l in label_names:
            ll.append(d["out"][l])
        X.append(fl)
        Y.append(ll)
    return np.array(X), np.array(Y)