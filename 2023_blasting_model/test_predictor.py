import numpy as np
import joblib

xs, ys, mlpr = joblib.load("IDEAL_ANFO_800_800_0_1.txt_scalers_mlpr.pkl")

radius = 0.10
ucs = 50e6
E = 10e9
density = 2500
friction = 45
bvol = 100
#feature order used in training.... ["UCS", "density", "friction", "radius", "E", "bvol"
features = np.array([ucs, density, friction, radius, E, bvol])

scaled_features = (features-xs.mean_)/xs.scale_
print(scaled_features)
result = mlpr.predict([scaled_features])
print(result)
scaled_result = result*ys.scale_ + ys.mean_
print(scaled_result)
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt", "v"]
for n, v in zip(label_names2, scaled_result[0]):
    print(n, v)
