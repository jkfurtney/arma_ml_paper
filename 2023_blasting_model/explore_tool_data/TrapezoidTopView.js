function TrapezoidTopView(destination, b, h, phi) {
  document.querySelectorAll(destination)[0].innerHTML ="";

  // Calculate top-down coordinates of the trapezoid
  // 1 ------ 2 ---- 5
  // |        |  /  /|
  // |        | /  / |
  // 3 ------ 4 ---- 6

  var x1 = 0;
  var x2 = b;
  var x3 = 0;
  var x4 = b;
  var x5 = b + h / Math.tan(phi * (Math.PI / 180));
  var x6 = x5;
  var y1 = 200;
  var y2 = y1;
  var y3 = 0;
  var y4 = 0;
  var y5 = y1;
  var y6 = 0;

// Create SVG element and set its size
  var svg = d3.select(destination)
              .append("svg")
              .attr("width", x5)
              .attr("height", y1);

 // Define the burden slope as a path
  var path = "M" + x1 + "," + y1 + " " +
                    "L" + x2 + "," + y2 + " " +
                    "L" + x5 + "," + y5 + " " +
                    "L" + x6 + "," + y6 + " " +
                    "L" + x4 + "," + y4 + " " +
                    "L" + x2 + "," + y2 + " " +
                    "L" + x4 + "," + y4 + " " +
                    "L" + x3 + "," + y3 + " " +
                    "Z";
  // Append the top shapes to the SVG
  svg.append("path")
     .attr("d", path)
     .style("fill", "RoyalBlue")
     .style("stroke", "black")
     .style("stroke-width", 1);

}
