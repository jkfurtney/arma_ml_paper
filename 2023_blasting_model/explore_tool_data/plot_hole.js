function plot_hole(destination, r0, r1, rplastic) {
  document.querySelectorAll(destination)[0].innerHTML ="";

  var margin = {top: 0, right: 0, bottom: 0, left: 0},
      width = 500 - margin.left - margin.right,
      height = width - margin.top - margin.bottom,
      dim = 1.0;

  var canvas = d3.select(destination)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("style", "overflow:hidden;")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  if (rplastic > dim) { dim = rplastic; }
  var x = d3.scaleLinear().range([0, width]).domain([0,2*dim]);

  function add_arc(innerRadius, outerRadius, color) {
    var arc = d3.arc().innerRadius(innerRadius).outerRadius(outerRadius)
      .startAngle(0).endAngle(-360 * (Math.PI/180.0));
    canvas.append("path").attr("d", arc)
      .attr("transform", "translate("+(width/2.0)+","+(height/2.0)+")")
      .attr("stroke", color).attr("fill", color);
  }

  add_arc(x(0), x(r1), 'grey');
  add_arc(x(r0), x(r0), 'black');
  add_arc(x(r1), x(rplastic), 'red');
  //add_arc(x(rplastic), x(rplastic), 'black');



  // add inner plot
  var x2 = d3.scaleLinear().range([0, width/2.0]);
  var y2 = d3.scaleLinear().range([0, height/2.0]);

  var xAxis = d3.axisBottom().scale(x2).ticks(5),
      yAxis = d3.axisLeft().scale(y2) .ticks(5).tickFormat(d3.format(".1e")),
      valueline = function(xa, ya, xscale, yscale){
        return d3.line()
          .x(function(d,i) { return xscale(xa[i]); })
          .y(function(d,i) { return yscale(ya[i]); })
        (Array(xa.length));
      };

  var inner_plot = canvas
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + width/2.0 + "," + 0 + ")");

  x2.domain([0,dim]);
  y2.domain([0,dim]);

  inner_plot.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height/2.0 + ")")
    .call(xAxis).append("text")
    .attr("transform", "translate(180, 20)")
    .attr("y", 5)
    .attr("dy", ".91em")
    .style("text-anchor", "end")
    .text("Radial Distance [m]");

    var legendData = [
    { label: "Original Hole", color: "black" },
    { label: "Expanded Hole", color: "grey" },
    { label: "Crushed Zone", color: "red" }
  ];

  var legend = canvas.append("g")
    .attr("class", "legend")
    .attr("transform", "translate(" + (width - 130) + "," + (height - 150) + ")");

  legend.append("rect")
  .attr("width", 140)
  .attr("height", 70)
  .attr("transform", "translate(-10,-350)")
  .attr("fill", "rgba(255, 255, 255, 0.8)")
  .attr("rx", 5)
  .attr("ry", 5);

  var legendRectSize = 18;
  var legendSpacing = 4;

  var legends = legend
  .selectAll(".legends")
  .data(legendData)
  .enter()
  .append("g")
  .attr("class", "legends")
  .attr("transform", function(d, i) {
    var legendHeight = legendRectSize + legendSpacing;
    var offset = legendHeight * legendData.length / 2;
    var horz = -0.2 * legendRectSize;
    var vert = i * legendHeight - (10.5 * offset);
    return "translate(" + horz + "," + vert + ")";
  });

legends
  .append("rect")
  .attr("width", legendRectSize)
  .attr("height", legendRectSize)
  .style("fill", function(d) {
    return d.color;
  });

legends
  .append("text")
  .attr("x", legendRectSize + legendSpacing)
  .attr("y", legendRectSize - legendSpacing)
  .text(function(d) {
    return d.label;
  });

}

function np_linspace (start,end,num,endpoint) {
    var local_endpoint = endpoint || false;
    var delta = end-start;
    var step = delta/num;
    var i=0;
    if (local_endpoint) {
        step = delta/(num-1);
    }
    ret = [];
    ret.push(start); // care is taken here to return exactly the start and end numbers
    for (i=1; i<num-1; i++) {
        ret.push(start+step*i);
    }
    if (local_endpoint) {
        ret.push(end);
    }
    else {
        ret.push(start+step*(num-1));
    }
    return ret;
}

function np_logspace (start,end,num,endpoint) {
  var local_endpoint = endpoint || false;
  var delta = end-start;
  var step = delta/num;
  var i=0;
  if (local_endpoint) {
    step = delta/(num-1);
  }
  ret = [];
  ret.push(Math.pow(10,start)); // care is taken here to return exactly the start and end numbers
  for (i=1; i<num-1; i++) {
    ret.push(Math.pow(10,(start+step*i)));
  }
  if (local_endpoint) {
    ret.push(Math.pow(10,end));
  }
  else {
    ret.push(Math.pow(10,start+step*(num-1)));
  }
  return ret;
}

function plot_pt(target, explosion_pressure, equilibrium_pressure, equilibrium_time) {
  document.querySelectorAll(target)[0].innerHTML ="";

  var margin = {top: 0, right: 0, bottom: 0, left: 20},
      width = 500 - margin.left - margin.right,
      height = width - margin.top - margin.bottom,
      dim = 1.0;

  var canvas = d3.select(target)
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("style", "overflow:hidden;")
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


  var x2 = d3.scaleLinear().range([0, width/2.0]);
  var y2 = d3.scaleLinear().range([0, height/2.0]);

  var xAxis = d3.axisBottom().scale(x2)
      .tickFormat(d3.format(".1e"))
      .ticks(3);
  var yAxis = d3.axisLeft().scale(y2)
      .tickFormat(d3.format(".1e"))
      .ticks(5);


  var valueline = function(xa, ya, xscale, yscale){
    return d3.line()
      .x(function(d,i) { return xscale(xa[i]); })
      .y(function(d,i) { return yscale(ya[i]); })
    (Array(xa.length));
  };


  var inner_plot = canvas
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
    .attr("transform", "translate(" + 100 + "," + 0 + ")");

  x2.domain([0,1e-3]);
  y2.domain([6.4e9,1e5]);

  inner_plot.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height/2.0 + ")")
    .call(xAxis);

  //Math.max(0,x2(0))
  inner_plot.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(0,0)")
    .call(yAxis);


  var x0 = np_linspace(0,equilibrium_time,50,true);
  var delta = explosion_pressure-equilibrium_pressure,
      factor = Math.log(0.0001)/equilibrium_time,
      y0 = x0.map(function (x) {return equilibrium_pressure+delta*(Math.exp(x*factor));});
  var line_colors = d3.scaleOrdinal(d3.schemeCategory10).domain(d3.range(10)),
      xa = x0,
      ya = y0;

      // xa = [1e-7, 1e-7, equilibrium_time],
      // ya = [1e5, explosion_pressure, equilibrium_pressure];
  inner_plot.append("path")
    .attr("class", "line")
    .attr("stroke", line_colors(0))
    .attr("d", valueline(xa,ya, x2, y2));

  inner_plot.append("text")
    .attr("x", width / 3 - 30)
    .attr("y", margin.top / 3 + 20)
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
    .style("font-weight", "bold");

  inner_plot.append("text")
    .attr("text-anchor", "middle")
    .attr("transform","translate("+(-width*0.16)+","+(height/4)+")rotate(-90)")
    .text("Pressure [Pa]").style("font-size", "18px");

  inner_plot.append("text")
    .attr("text-anchor", "middle")
    .attr("transform", "translate("+ (width/4)+","+(height/1.6)+")")
    .text("Time [s]").style("font-size", "18px");

}
