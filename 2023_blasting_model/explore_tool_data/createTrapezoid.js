function createTrapezoid(destination, b, h, phi,charge_fraction, hole_diameter) {
  document.querySelectorAll(destination)[0].innerHTML ="";

  // Calculate coordinates of the trapezoid
  //  (x1, y1) __________ (x2, y2)
  //       |               \
  //       |                \
  //  (x3, y3) _____________ (x4, y4)
  var x1 = 0;
  var x2 = b;
  var x3 = 0;
  var x4 = h / Math.tan(phi * (Math.PI / 180)) + b;
  var y1 = h;
  var y2 = h;
  var y3 = 0;
  var y4 = 0;

  // charge fraction size
  var rectWidth = hole_diameter;  // Adjust the width of the rectangle as desired
  var rectHeight = h * (1-charge_fraction);  // Adjust the height of the rectangle as desired

// Create SVG element and set its size
  var svg = d3.select(destination)
              .append("svg")
              .attr("width", x4)
              .attr("height", y1);

  // Define the trapezoid shape as a path
  var path = "M" + x3 + "," + y3 + " " +
                    "L" + x1 + "," + y1 + " " +
                    "L" + x4 + "," + y2 + " " +
                    "L" + x2 + "," + y4 + " " +
                    "Z";

  // Append the trapezoid shape to the SVG
  svg.append("path")
     .attr("d", path)
     .style("fill", "RoyalBlue")
     .style("stroke", "black")
     .style("stroke-width", 1);

  // adding explosive under hole
  svg.append("rect")
      .attr("x", x3)
     .attr("y", y3)
      .attr("width", rectWidth)
      .attr("height", y1)
     .style("fill", "indianred")
     .style("stroke", "black")
     .style("stroke-width", 1);

  // adding hole over explosive
  svg.append("rect")
    .attr("x", x3)
    .attr("y", y3)
    .attr("width", rectWidth)
    .attr("height", rectHeight)
    .style("fill", "white")
    .style("stroke", "black")
    .style("stroke-width", 1);
}
