function plot_bar_chart(data) {
  document.querySelectorAll("#barchart")[0].innerHTML ="";
  var margin = {top: 20, right: 10, bottom: 70, left: 80},
      width = 300 - margin.left - margin.right+150,
      height = width - margin.top - margin.bottom;

  //var x = d3.scaleOrdinal().range([0, width]);

  var x = d3.scaleBand()
    .range([ 0, width ])
      .domain(data.map(function(d) { return d.ename; }))
      .padding(0.2);
  var y = d3.scaleLinear().range([height, 0]);
  var xAxis = d3.axisBottom().scale(x);
  var yAxis = d3.axisLeft()
      .scale(y)
      .ticks(10).tickFormat(d3.format(".1e"));

  var svg = d3.select("#barchart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
          "translate(" + margin.left + "," + margin.top + ")");

  //d3.max(data, function(d) { return d.enumber; })
  x.domain(data.map(function(d) { return d.ename; }));
  y.domain([0, 3e7]);

  // add title
  svg.append("text")
    .attr("x", (width / 2))
    .attr("y", (margin.top / 2))
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
      .style("font-weight", "bold");

  // add axis
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .selectAll("text")
    .style("text-anchor", "center")
    .style("font-size", "16px");

 svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);

  svg.append("text")
   .attr("transform", "rotate(-90)")
   .attr("y", 0 - margin.left)
   .attr("x", 0 - (height / 2))
   .attr("dy", "1em")
   .style("text-anchor", "middle")
   .text("Energy [J/m]");

  // Add bar chart
  svg.selectAll("bar")
    .data(data)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d.ename); })
    .attr("width", 100)
    .attr("y", function(d) { return y(d.enumber); })
    .attr("height", function(d) { return height - y(d.enumber); })
    .attr("fill", "#69b3a2")
      .attr("stroke", "black", "stroke-width", 2);
}
