import numpy as np
import math
from isee_burden_model import simple_model
from load_syntheric_data import read_JSON_to_XY

N = 10_000

# bench height 3 to 30 m
# burden 1 to 15 m
# max burden area 15*15 225 m2 lowest is 1 m2
# since this is one dimensional we can just use a single linear space, no lhc needed.
min_face_angle = np.deg2rad(60)  # [deg]
burden_min, burden_max = 1.0, 15.0
charge_fraction_min, charge_fraction_max = 0.4, 1.0
height_max = 30 # [m] max bench height

# these are areas
min_vol = burden_min*burden_min/charge_fraction_max
max_vol = (burden_max*burden_max + burden_max*height_max/2/math.tan(min_face_angle))/charge_fraction_max
bvol = np.linspace(min_vol, max_vol, 10000)
# loop over each explosive type
# load the corresponding JSON file to get X and Y
# calculate the throw velocity for each case (10000 cases)
# add the throw velocity to the Y vector and save it as a .npy file.
# each explosive file will have an X and Y npy file.

explosive_files = ["IDEAL_ANFO_800_800_0_1.txt"]

model = simple_model()
model.explosive_file = explosive_files[0]
model.p_eq = 135_147_548.0
model.burden = 4.0
model.rock_density = 3_000.0
model.explosive_diameter = model.hole_diameter = 0.15
model.hole_length = 6
model.bench_height = 10
model.setup()
#model.burden_mass = model.rock_density*bvol[i]
model.solve()
model.vel[-1]
