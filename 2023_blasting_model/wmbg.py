from cmath import *
from scipy.optimize import fminbound
from scipy.integrate import quad

class WilliamsburgEoS:
    """
    Williamsburg equation of state for detonation reaction
    products. This class reads Vixen ideal files as input. Methods are
    available to calculate dimensional pressure from reduced volume
    and to give expansion energy.
    """
    def __init__(self, fileName):
        """
        Call constructor with the Vixen ideal file name
        """
        self.ucj             = 0.0
        self.vcj             = 0.0
        self.alpha     = [complex(0.0, 0.0) for x in range(4)]
        self.beta      = [complex(0.0, 0.0) for x in range(4)]
        self.gamma     = [complex(0.0, 0.0) for x in range(4)]
        self.density         = 0.0 # initial explosive density
        self._readDetFile(fileName)
        self.name = fileName

    def _readDetFile(self,fn):
        lines          = open(fn,"r").readlines()
        self.density   = float(lines[1].split()[1])
        self.vcj       = float(lines[6].split()[1])
        self.ucj       = float(lines[7].split()[1])
        self.vod       = float(lines[4].split()[1])
        for i in range(4):
            self.alpha[i] = complex(float(lines[8+i].split()[0]),
                                    float(lines[8+i].split()[1]))
            self.beta[i]  = complex(float(lines[13+i].split()[0]),
                                    float(lines[13+i].split()[1]))
            self.gamma[i] = complex(float(lines[18+i].split()[0]),
                                    float(lines[18+i].split()[1]))
        self.gamma0          = complex(1.0,0.0)
        self.ginf            = complex(1.0,0.0)
        for i in range(4):
            self.gamma0 += self.alpha[i]*self.gamma[i]
            self.ginf   += self.gamma[i]*(complex(1.0,0.0) + self.alpha[i])

    def reducedVolumeAtExplosionPressure(self):
        """
        Return the reduced volume at the explosion pressure
        (initial explosive density)
        """
        return 1.0/self.density/self.vcj

    def reducedVolumeAtPressure(self,p):
        """
        Return the reduced volume at a given pressure
        """
        return fminbound(lambda rv : abs(p - self(rv)), 1 , 10000)

    def __call__(self, reducedVolume):
        """
        Calling this object with a reduced volume returns a
        dimensional pressure. rv = r1**2/r0**2 for a cylinder
        """
        v   = complex(reducedVolume,0.0)
        se  = complex(0.0, 0.0)
        g   = self.gamma0
        for i in range(4):
            d   =   complex(1.0,0.0) + self.beta[i]*v
            t1  =   self.gamma[i]/d
            g   +=  t1
            se  +=  self.gamma[i]*log(d/(complex(1.0,0.0)+self.beta[i]))
        u = (exp(se)*(complex(1.0,0.0)/v)**(self.ginf-complex(1.0,0.0))).real
        p = ((g-complex(1.0,0.0))*u/v).real
        dimensionalPressure = p*self.ucj/self.vcj
        return dimensionalPressure

    def expansionEnergyPerUnitVolume(self, rv0, rv1):
        """
        rv0: Reduced volume at start of expansion
        rv1: Reduced volume at end of expansion

        Return energy per unit volume (J/m^3) done by expanding
        reaction products from rv0 to rv1. Multiply return value by
        volume at rv0 to get expansion work.
        """
        assert rv0 >= 1.0
        assert rv0 < rv1
        volcj = 1.0/rv0
        return quad(lambda vol : self(vol/volcj),1,volcj*rv1)[0]




if __name__ == '__main__':
    pass
    # fix this test code
