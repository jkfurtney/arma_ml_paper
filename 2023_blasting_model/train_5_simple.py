
import numpy as np
import pandas as pd
import matplotlib; matplotlib.rcParams["savefig.directory"] = "."
from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})
import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import json
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
import joblib
from load_syntheric_data import read_JSON_to_XY
from avp import show_actual_predicted

feature_names = ["UCS", "density", "friction", "radius", "E"]
feature_names2 = ["UCS", "rho", "phi", "r", "E"]

label_names = ["equilibrium_pressure", "final_radius", "plastic_radius", "plasticity", "gas", "vibrations", "eqt"]
label_names2 = ["eqp", "r_f", "r_p", "plas", "gas", "vibe", "eqt"]


data_filename = "../blasting_5_unknowns/five_var_run_IDEAL_HeavyANFO80%prill_1000_0_100.txt.txt"
X,Y = read_JSON_to_XY(data_filename, feature_names, label_names)

Y[:,1] /= X[:,3] # normalize final radius
Y[:,2] /= X[:,3] # normalize plastic radius

for i, fn in enumerate(feature_names):
    print(fn, X[:,i].min(), X[:,i].max())

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, random_state=2)

scaler = skl.preprocessing.StandardScaler()
yscaler = skl.preprocessing.StandardScaler()
scaler.fit(X_train)
yscaler.fit(Y_train)

X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)
y_train_scaled = yscaler.transform(Y_train)
y_test_scaled = yscaler.transform(Y_test)

hl_size = 15
mlpr = MLPRegressor(hidden_layer_sizes=(hl_size,hl_size,hl_size),
                    activation='tanh',
                    solver='lbfgs',
                    alpha=1e-4,
                    max_iter=4*1600,
                    random_state=1,

                    # activation='logistic',
                    # solver='sgd',
                    # learning_rate='adaptive',
                    # learning_rate_init=0.1,
                    # momentum=0.95,
                    # max_iter=5000
                    )

mlpr.fit(X_train_scaled, y_train_scaled)


ts = mlpr.score(X_train_scaled, y_train_scaled)
vs = mlpr.score(X_test_scaled, y_test_scaled)
print(ts, vs)

Y_predicted = yscaler.inverse_transform(mlpr.predict(X_test_scaled))
Y_actual = yscaler.inverse_transform(y_test_scaled)

for i, name in enumerate(label_names):
    print(i, name)
    show_actual_predicted(data_filename[23:-4], name, Y_actual[:,i], Y_predicted[:,i])

#

