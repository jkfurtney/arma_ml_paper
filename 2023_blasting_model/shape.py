# heuristic model for muckpile shape


import numpy as np
from scipy.integrate import simpson
from numpy import trapz
import matplotlib; matplotlib.rcParams["savefig.directory"] = "."
from matplotlib import pyplot as plt
plt.rcParams.update({'font.size': 18})

g = 9.8  # gravity m/s/s

h = 10.0  # bench height in M
b = 3  # burden in M
v = 2  # throw velocity M/s

x = np.linspace(0, b, 50)
y = np.linspace(0, h, 50)
dx = b/len(x)
dy = h/len(y)
X, Y = np.meshgrid(x, y)

angle = np.deg2rad(70)
X += X/b*(h-Y)/np.tan(angle)
toe_position = h/np.tan(angle)+b

#change in throe velocity with vertical position
v_y = lambda z: -4*((z/h)-1/2)**2+1
v_y = lambda z: np.ones_like(z)
# def v_y(z):
#     z=z/h
#     lower_mask = z<0.5
#     m = 8/5 * 2
#     ret = np.zeros_like(z)
#     ret[lower_mask] = z[lower_mask] * m
#     ret[~lower_mask] = m*(0.5-(.5*(z[~lower_mask]-0.5)))
#     return ret

plt.plot(v_y(y), y)
plt.show()


d = v * v_y(Y) * np.sqrt(2*Y/g) + X
# d is the position each point travel to in the horizontal direction

plt.scatter(X, Y, c=d)
plt.gca().set_aspect(1)
plt.plot([0, 0, b, toe_position], [0, h, h, 0])
plt.axvline(0)
plt.colorbar()
plt.show()

weights = 1+(h-Y)/np.tan(angle)/b  # how much fractional volume does each node represent?
node_area = dx * dy * weights
bins = np.linspace(d.min(), d.max(), 100)
height = np.zeros_like(bins)
bin_width = ((d.max()+1e-7)-d.min())/len(bins)
for distance, area in zip((d-d.min()).flat, node_area.flat):
    i = int(distance/bin_width)
    dh = area/bin_width
    height[i] += dh

plt.plot(bins, height)
kernel_size = 10
kernel = np.ones(kernel_size) / kernel_size
data_convolved = np.convolve(height, kernel, mode='same')
plt.plot(bins, data_convolved)
plt.plot([0, 0, b, toe_position], [0, h, h, 0])
plt.gca().set_aspect(1)
plt.show()

area_burden = b*h + h**2/np.tan(angle)/2
print('burden area =', area_burden)
area_t = trapz(height, dx=bin_width)
area_s = simpson(height, dx=bin_width)
print("trapz area =", area_t)
print("simpson area =", area_s)
