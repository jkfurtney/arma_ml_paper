# sklearn_porter is not compatible with the current versions of sklearn or Python
# the following code is a monkey patch to make it work at runtime so modification of the
# sklearn_porter files is not needed.
import sklearn
import sklearn.tree
import sklearn.ensemble
import sklearn.svm
import sklearn.neighbors
import sklearn.neural_network
import json
import sys
sys.modules["sklearn.tree.tree"] = sklearn.tree
sys.modules["sklearn.ensemble.weight_boosting"] = sklearn.ensemble
sys.modules["sklearn.ensemble.forest"] = sklearn.ensemble
sys.modules["sklearn.svm.classes"] = sklearn.svm
sys.modules["sklearn.neighbors.classification"] = sklearn.neighbors
sys.modules["sklearn.neural_network.multilayer_perceptron"] = sklearn.neural_network
json.origional_load = json.load
json.load = lambda f, **kwargs: json.origional_load(f)
from sklearn_porter import Porter
import joblib
import numpy as np
import jinja2
from pathlib import Path
from glob import glob


def mlpr_to_JavaScript(name, mlpr):
    porter = Porter(mlpr, language='js')
    output = porter.export(embed_data=False)
    lines = output.split("\n")

    # the Porter puts in a bunch of lines that cause a problem
    # here we programmatically remove them to make the process more reproducible and
    # more automated
    lines_to_skip = ["if (typeof process",  "if (process", "var features", "var prediction", "console.log", "// Prediction:"]
    keep_lines = []
    for line in lines:
        l = line.strip()
        keep = True
        for lts in lines_to_skip:
            if l.startswith(lts):
                keep=False
        if keep:
            keep_lines.append(line)

    # also we need to drop the last two lines
    keep_lines = keep_lines[:-2]

    js_mlpr_code = slice(0,82) # the first 82 lines are the code for the nn evaluation
    js_layers = 86
    js_weights = 87
    js_bias = 88
    js_mlpr_object = 90

    mlpr_code = "\n".join(keep_lines[js_mlpr_code])
    layers = keep_lines[js_layers].replace("layers", f"{name}_layers")
    weights = keep_lines[js_weights].replace("weights", f"{name}_weights")
    bias = keep_lines[js_bias].replace("bias", f"{name}_bias")
    mlpr_object = keep_lines[js_mlpr_object].replace("reg", f"{name}_reg").replace("layers", f"{name}_layers").replace("bias", f"{name}_bias").replace("weights", f"{name}_weights")
    return mlpr_code, layers, weights, bias, mlpr_object

def scaler_to_javascript(name, scaler):
    template = """
    var {}_scale_mean = {},
        {}_scale_scale = {};
    """
    ppa = lambda _ : np.array2string(_, separator=",")
    return template.format(name, ppa(scaler.mean_), name, ppa(scaler.scale_))

datafiles = ("IDEAL_ANFO_800_800_0_1.txt_scalers_mlpr.pkl",
             "IDEAL_Surface_Emulsion_1150_1150_0_1.txt_scalers_mlpr.pkl",
             "IDEAL_DE35AT1200_1200_0_1.txt_scalers_mlpr.pkl",
             "IDEAL_HeavyANFO80%prill_1000_0_100.txt_scalers_mlpr.pkl")

short_names = ("ANFO", "SE", "DE", "HA")

with open("explore_tool_data/model.js", "w") as f:
    for i, (short_name, filename) in enumerate(zip(short_names, datafiles)):
        scaler, yscaler, mlpr = joblib.load(filename)
        mlpr_code, layers, weights, bias, mlpr_object = mlpr_to_JavaScript(short_name, mlpr)
        xscaler_code = scaler_to_javascript(f"x_{short_name}_", scaler)
        yscaler_code = scaler_to_javascript(f"y_{short_name}_", yscaler)
        if i==0:
            print(mlpr_code, file=f)
        print(layers, file=f)
        print(weights, file=f)
        print(bias, file=f)
        print(mlpr_object, file=f)
        print(xscaler_code, file=f)
        print(yscaler_code, file=f)
