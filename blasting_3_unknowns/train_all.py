import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
from sklearn.model_selection import train_test_split
from learning_curve import train_and_test

X = np.load("Xrun_IDEAL_ANFO_800_800_0_1.txt.txt.npy")
Y = np.load("Yrun_IDEAL_ANFO_800_800_0_1.txt.txt.npy")

x_train, x_test, y_train, y_test = train_test_split(X, Y,
                                                    test_size=0.2,
                                                    random_state=2)

for i in range(4):
    model, ts, vs = train_and_test(x_train, y_train, x_test, y_test, target=i)
    print (i, vs)
