import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor


lhc_sizes = (4,5,6,7,8,9,10,11,12,13,14,16)
sizes = np.cumsum(2**np.array((4,5,6,7,8,9,10,11,12,13,14,16)))

X_train, X_test = [], []
Y_train, Y_test = [], []
test_score, validation_score = [], []

def train_and_test(X_train, y_train, X_test, y_test):
    scaler = skl.preprocessing.StandardScaler()
    yscaler = skl.preprocessing.StandardScaler() #PowerTransformer() #StandardScaler()
    scaler.fit(X_train)
    yscaler.fit(y_train.reshape(-1,1))

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_train_scaled = yscaler.transform(y_train)
    y_test_scaled = yscaler.transform(y_test)

    mlpr = MLPRegressor(hidden_layer_sizes=(20,20,20,15),
                        activation='tanh',
                        solver='lbfgs',
                        max_iter=2*1600,
                        random_state=2)

    mlpr.fit(X_train_scaled, y_train_scaled)
    ts = mlpr.score(X_train_scaled, y_train_scaled)
    vs = mlpr.score(X_test_scaled, y_test_scaled)
    return mlpr, ts, vs



for total_size, lhc_size in zip(sizes,lhc_sizes):
    print(f"training {lhc_size}")
    data = np.load(f"lhc_{lhc_size}.npy")
    X = data[:, :10]
    Y = data[:, -2:]
    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=2)
    X_train += x_train.tolist()
    X_test += x_test.tolist()
    Y_train += y_train.tolist()
    Y_test += y_test.tolist()

    _, ts, vs = train_and_test(np.array(X_train), np.array(Y_train),
                                   np.array(X_test), np.array(Y_test))
    test_score.append(ts)
    validation_score.append(vs)
    print(total_size, ts, vs)
