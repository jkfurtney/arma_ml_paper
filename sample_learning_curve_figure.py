import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
from scipy.interpolate import interp1d
plt.rcParams.update({'font.size': 18})


soil_12 = np.array(([496, 1008, 2032, 4080, 8176, 16368, 32752, 65520], [0.36023307171698615, 0.4797212402108624, 0.6764619866015984, 0.7654628974058908, 0.931260905270206, 0.9861287555449563, 0.9921199517138651, 0.9936093312454417]))

plt.semilogx(soil_12[0], -np.log10(1-soil_12[1]), "o-")
plt.axhline(-np.log10(1-0.9), color="black")
plt.axhline(-np.log10(1-0.99), color="black")
plt.axhline(-np.log10(1-0.999), color="black")

bbox = dict(boxstyle='round', facecolor='white')

plt.text(400, 1.0, "$R^2=0.9$", bbox=bbox, verticalalignment="center")
plt.text(400, 2.02, "$R^2=0.99$", bbox=bbox, verticalalignment="center")
plt.text(400, 3.02, "$R^2=0.999$", bbox=bbox, verticalalignment="center")

plt.ylabel("Model Score $(-log_{10}(1-R^2))$ []")
plt.xlabel("Dataset Size []")

plt.show()
